<?php

/**
 * Implements hook_drush_command().
 */
function drush_process_test_drush_command() {
  $items = array();
   
  $items['drush-process-test-sleep'] = array(
    'description' => "Sleeps for the specified period of time.",
    'arguments' => array(
      'sleep_time' => dt('The time to sleep for'),
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  
  $items['drush-process-test-sleep-wrapper'] = array(
    'description' => "Sleeps for the specified period of time in a subprocess.",
    'arguments' => array(
      'sleep_time' => dt('The time to sleep for'),
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  
  $items['drush-process-test-pi'] = array(
    'description' => "Approximates pi.",
    'arguments' => array(
      'iterations' => dt('The number of iterations to use to compute pi'),
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  
  $items['drush-process-test-pi-wrapper'] = array(
    'description' => "Approximates pi.",
    'arguments' => array(
      'iterations' => dt('The number of iterations to use to compute pi'),
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  $items['drush-process-test-packet'] = array(
    'description' => "Returns a packet of the given size.",
    'arguments' => array(
      'size' => dt('The size of packet to generate.'),
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  $items['drush-process-test-packet-wrapper'] = array(
    'description' => "Returns a packet of the given size in a subprocess.",
    'arguments' => array(
      'size' => dt('The size of packet to generate.'),
    ),
    'required-arguments' => TRUE,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
   
  return $items;
}

/**
 * Drush command that just sleeps for some time.
 */
function drush_drush_process_test_sleep($sleep_time) {
  sleep($sleep_time);
}

/**
 * Drush command to call the 'drush-process-test-sleep' command.
 */
function drush_drush_process_test_sleep_wrapper($sleep_time) {
  drush_invoke_process('@self', 'drush-process-test-sleep', array($sleep_time));
}

/**
 * Drush command to compute pi.
 */
function drush_drush_process_test_pi($iterations) {
  drush_print_r(bcpi(100, $iterations));
}

/**
 * Drush command to call the 'drush-process-test-pi' command.
 */
function drush_drush_process_test_pi_wrapper($iterations) {
  drush_invoke_process('@self', 'drush-process-test-pi', array($iterations));
}

/**
 * Compute a approximation to pi.
 *
 * See: http://forum.junowebdesign.com/share-php-script/24692-calculate-pi-into-very-high-precision.html
 */
function bcpi($precision=30, $accuracy=21){
  bcscale($precision);
  $n = 1;
  $bcatan1 = 0;
  $bcatan2 = 0;
  while($n < $accuracy){
  //atan functions
    $bcatan1 = bcadd($bcatan1, bcmul(bcdiv(pow(-1, $n+1), $n * 2 - 1), bcpow(0.2, $n * 2 -1)));
    $bcatan2 = bcadd($bcatan2, bcmul(bcdiv(pow(-1, $n+1), $n * 2 - 1), bcpow(bcdiv(1,239), $n * 2 -1)));
    ++$n;
  }
  return    bcmul(4,bcsub(bcmul(4, $bcatan1),$bcatan2));
}

/**
 * Drush command that just sleeps for some time.
 */
function drush_drush_process_test_packet($size) {
  $string = _user_password($size);
  drush_log($string, 'info');
  sleep(1);
  drush_log($string, 'info');
}

/**
 * Drush command to call the 'drush-process-test-sleep' command.
 */
function drush_drush_process_test_packet_wrapper($size) {
  drush_invoke_process('@self', 'drush-process-test-packet', array($size));
}

function _user_password($length = 10) {
  // This variable contains the list of allowable characters for the
  // password. Note that the number 0 and the letter 'O' have been
  // removed to avoid confusion between the two. The same is true
  // of 'I', 1, and 'l'.
  $allowable_characters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';

  // Zero-based count of characters in the allowable list:
  $len = strlen($allowable_characters) - 1;

  // Declare the password as a blank string.
  $pass = '';

  // Loop the number of times specified by $length.
  for ($i = 0; $i < $length; $i++) {

    // Each iteration, pick a random character from the
    // allowable string and append it to the password:
    $pass .= $allowable_characters[mt_rand(0, $len)];
  }

  return $pass;
}